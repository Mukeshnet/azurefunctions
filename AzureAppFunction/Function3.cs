using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using System.Xml.Serialization;
using System.IO;
using System.Data.SqlClient;
using System.Data;

namespace AzureAppFunction
{
    public static class Function3
    {
        [FunctionName("Function3")]
        public static async Task<HttpResponseMessage> Run([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            log.Info("C# HTTP trigger function processed a request.");

            string connectionString = "Data Source= 54.85.106.196; Initial Catalog= lj_link; User ID=larson; pwd=larson; Integrated Security=False";
            // parse query parameter
          
            var userID = req.Headers.GetValues("UserID").FirstOrDefault();

            var content = req.Content;
            string jsonContent = content.ReadAsStringAsync().Result;
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(req.Content.ReadAsStreamAsync().Result);
            var str = xmlDoc.InnerXml;
            XmlSerializer serializer = new XmlSerializer(typeof(PurchaseOrderDetails.PurchaseOrders));
            int newID;
            StringReader rdr1 = new StringReader(str);
            var resultingMessage = (PurchaseOrderDetails.PurchaseOrders)serializer.Deserialize(rdr1);
            string query = "INSERT INTO dbo.PurchaseOrderHeader (FULFILLMENT_CENTER_ID, SUPPLIER_ID, STATUS, PURCHASE_ORDER_NUMBER, SHIPPING_CHARGE,CRATING_COST,ACTION,TenantID,CompanyID,UserID) " +
                           "VALUES ( @FULFILLMENT_CENTER_ID, @SUPPLIER_ID, @STATUS, @PURCHASE_ORDER_NUMBER, @SHIPPING_CHARGE,@CRATING_COST,@ACTION,@TenantID,@CompanyID,@UserID) ; SELECT CAST(scope_identity() AS int)";

            // create connection and command
            using (SqlConnection cn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(query, cn))
            {
                // define parameters and their values
                cmd.Parameters.Add("@FULFILLMENT_CENTER_ID", SqlDbType.VarChar, 50).Value = resultingMessage.PurchaseOrderHeader.FULFILLMENT_CENTER_ID;
                cmd.Parameters.Add("@SUPPLIER_ID", SqlDbType.VarChar, 50).Value = resultingMessage.PurchaseOrderHeader.SUPPLIER_ID;
                cmd.Parameters.Add("@STATUS", SqlDbType.VarChar, 50).Value = resultingMessage.PurchaseOrderHeader.STATUS;
                cmd.Parameters.Add("@PURCHASE_ORDER_NUMBER", SqlDbType.VarChar, 50).Value = resultingMessage.PurchaseOrderHeader.PURCHASE_ORDER_NUMBER;
                cmd.Parameters.Add("@SHIPPING_CHARGE", SqlDbType.VarChar, 50).Value = resultingMessage.PurchaseOrderHeader.SHIPPING_CHARGE;
                cmd.Parameters.Add("@ACTION", SqlDbType.VarChar, 10).Value = resultingMessage.PurchaseOrderHeader.ACTION;
                cmd.Parameters.Add("@TenantID", SqlDbType.VarChar, 50).Value = "TestT";
                cmd.Parameters.Add("@CompanyID", SqlDbType.VarChar, 50).Value = "TestC";
                cmd.Parameters.Add("@UserID", SqlDbType.VarChar, 50).Value = "User";
                cmd.Parameters.Add("@CRATING_COST", SqlDbType.VarChar, 50).Value = resultingMessage.PurchaseOrderHeader.CRATING_COST;
                // open connection, execute INSERT, close connection
                cn.Open();
                newID = (int)cmd.ExecuteScalar();              
                cn.Close();


                StringReader rdr = new StringReader(str);
           
                System.Data.DataSet ds = new System.Data.DataSet();
                System.Data.DataTable dt = new System.Data.DataTable();
                ds.ReadXml(rdr);                
                dt = ds.Tables[1];
                DataColumn newColumn = new DataColumn("PurchaseOrderHeaderID", typeof(System.String));
                newColumn.DefaultValue = newID;
                dt.Columns.Add(newColumn);
                SqlBulkCopy objbulk = new SqlBulkCopy(connectionString);
                //assigning Destination table name  
                objbulk.DestinationTableName = "PurchaseOrderDetails";
                //Mapping Table column  
                objbulk.ColumnMappings.Add("PurchaseOrderHeaderID", "PurchaseOrderHeaderID");
                objbulk.ColumnMappings.Add("PRODUCT", "PRODUCT");
                objbulk.ColumnMappings.Add("SHIPMENT_NUMBER", "SHIPMENT_NUMBER");
                objbulk.ColumnMappings.Add("STATUS", "STATUS");
                objbulk.ColumnMappings.Add("QUANTITY_EXPECTED", "QUANTITY_EXPECTED");
                objbulk.ColumnMappings.Add("QUANTITY_ALREADY_RECEIVED", "QUANTITY_ALREADY_RECEIVED");
                objbulk.ColumnMappings.Add("UNIT_OF_MEASURE_CODE", "UNIT_OF_MEASURE_CODE");
                objbulk.ColumnMappings.Add("PRICE", "PRICE");                
                objbulk.WriteToServer(dt);
                objbulk.Close();
            }
            string result = "Succcessfully inserted.PurchaseOrderHeaderID = " + newID;



            return req.CreateResponse(HttpStatusCode.OK, result);
        }
    

    }
}
