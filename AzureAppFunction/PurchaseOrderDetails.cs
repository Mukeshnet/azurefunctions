﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureAppFunction
{
    public class PurchaseOrderDetails
    {

        // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class PurchaseOrders
        {

            private PurchaseOrdersPurchaseOrderHeader purchaseOrderHeaderField;

            /// <remarks/>
            public PurchaseOrdersPurchaseOrderHeader PurchaseOrderHeader
            {
                get
                {
                    return this.purchaseOrderHeaderField;
                }
                set
                {
                    this.purchaseOrderHeaderField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class PurchaseOrdersPurchaseOrderHeader
        {

            private uint fULFILLMENT_CENTER_IDField;

            private byte sUPPLIER_IDField;

            private uint pURCHASE_ORDER_NUMBERField;

            private string sTATUSField;

            private object tYPEField;

            private object bUYING_TERMSField;

            private object fOBField;

            private object sHIP_VIAField;

            private object qUOTE_REFERENCE_NUMBERField;

            private decimal sHIPPING_CHARGEField;

            private byte cRATING_COSTField;

            private object dELIVER_BYField;

            private object cANCELLATION_DATEField;

            private byte eSTIMATED_DELIVERY_DATEField;

            private object sHIPPED_DATEField;

            private object pORT_ENTRY_DATEField;

            private object cUSTOMS_CLEAR_DATEField;

            private object dELIVER_BY_DATEField;

            private object vESSEL_NAMEField;

            private object sHIPPER_NAMEField;

            private object sHIPPER_TYPEField;

            private object vOYAGE_NUMBERField;

            private object fLIGHT_NUMBERField;

            private object sEAL_NUMBERField;

            private object cONTAINER_NUMBERField;

            private object tRUCK_SIZEField;

            private object cONTAINER_TYPEField;

            private object lOADING_PORTField;

            private object iNVOICE_NUMBERField;

            private string aWB_NUMBERField;

            private object bILL_OF_LADINGField;

            private object cARRIER_IDField;

            private object pRO_NUMBERField;

            private object rECEIVING_TYPEField;

            private object pRIORITY_CODEField;

            private object sPECIAL_INSTRUCTIONSField;

            private string aCTIONField;

            private object lAST_ACTIVITY_DATEField;

            private PurchaseOrdersPurchaseOrderHeaderPurchaseOrderDetails[] purchaseOrderDetailsField;

            /// <remarks/>
            public uint FULFILLMENT_CENTER_ID
            {
                get
                {
                    return this.fULFILLMENT_CENTER_IDField;
                }
                set
                {
                    this.fULFILLMENT_CENTER_IDField = value;
                }
            }

            /// <remarks/>
            public byte SUPPLIER_ID
            {
                get
                {
                    return this.sUPPLIER_IDField;
                }
                set
                {
                    this.sUPPLIER_IDField = value;
                }
            }

            /// <remarks/>
            public uint PURCHASE_ORDER_NUMBER
            {
                get
                {
                    return this.pURCHASE_ORDER_NUMBERField;
                }
                set
                {
                    this.pURCHASE_ORDER_NUMBERField = value;
                }
            }

            /// <remarks/>
            public string STATUS
            {
                get
                {
                    return this.sTATUSField;
                }
                set
                {
                    this.sTATUSField = value;
                }
            }

            /// <remarks/>
            public object TYPE
            {
                get
                {
                    return this.tYPEField;
                }
                set
                {
                    this.tYPEField = value;
                }
            }

            /// <remarks/>
            public object BUYING_TERMS
            {
                get
                {
                    return this.bUYING_TERMSField;
                }
                set
                {
                    this.bUYING_TERMSField = value;
                }
            }

            /// <remarks/>
            public object FOB
            {
                get
                {
                    return this.fOBField;
                }
                set
                {
                    this.fOBField = value;
                }
            }

            /// <remarks/>
            public object SHIP_VIA
            {
                get
                {
                    return this.sHIP_VIAField;
                }
                set
                {
                    this.sHIP_VIAField = value;
                }
            }

            /// <remarks/>
            public object QUOTE_REFERENCE_NUMBER
            {
                get
                {
                    return this.qUOTE_REFERENCE_NUMBERField;
                }
                set
                {
                    this.qUOTE_REFERENCE_NUMBERField = value;
                }
            }

            /// <remarks/>
            public decimal SHIPPING_CHARGE
            {
                get
                {
                    return this.sHIPPING_CHARGEField;
                }
                set
                {
                    this.sHIPPING_CHARGEField = value;
                }
            }

            /// <remarks/>
            public byte CRATING_COST
            {
                get
                {
                    return this.cRATING_COSTField;
                }
                set
                {
                    this.cRATING_COSTField = value;
                }
            }

            /// <remarks/>
            public object DELIVER_BY
            {
                get
                {
                    return this.dELIVER_BYField;
                }
                set
                {
                    this.dELIVER_BYField = value;
                }
            }

            /// <remarks/>
            public object CANCELLATION_DATE
            {
                get
                {
                    return this.cANCELLATION_DATEField;
                }
                set
                {
                    this.cANCELLATION_DATEField = value;
                }
            }

            /// <remarks/>
            public byte ESTIMATED_DELIVERY_DATE
            {
                get
                {
                    return this.eSTIMATED_DELIVERY_DATEField;
                }
                set
                {
                    this.eSTIMATED_DELIVERY_DATEField = value;
                }
            }

            /// <remarks/>
            public object SHIPPED_DATE
            {
                get
                {
                    return this.sHIPPED_DATEField;
                }
                set
                {
                    this.sHIPPED_DATEField = value;
                }
            }

            /// <remarks/>
            public object PORT_ENTRY_DATE
            {
                get
                {
                    return this.pORT_ENTRY_DATEField;
                }
                set
                {
                    this.pORT_ENTRY_DATEField = value;
                }
            }

            /// <remarks/>
            public object CUSTOMS_CLEAR_DATE
            {
                get
                {
                    return this.cUSTOMS_CLEAR_DATEField;
                }
                set
                {
                    this.cUSTOMS_CLEAR_DATEField = value;
                }
            }

            /// <remarks/>
            public object DELIVER_BY_DATE
            {
                get
                {
                    return this.dELIVER_BY_DATEField;
                }
                set
                {
                    this.dELIVER_BY_DATEField = value;
                }
            }

            /// <remarks/>
            public object VESSEL_NAME
            {
                get
                {
                    return this.vESSEL_NAMEField;
                }
                set
                {
                    this.vESSEL_NAMEField = value;
                }
            }

            /// <remarks/>
            public object SHIPPER_NAME
            {
                get
                {
                    return this.sHIPPER_NAMEField;
                }
                set
                {
                    this.sHIPPER_NAMEField = value;
                }
            }

            /// <remarks/>
            public object SHIPPER_TYPE
            {
                get
                {
                    return this.sHIPPER_TYPEField;
                }
                set
                {
                    this.sHIPPER_TYPEField = value;
                }
            }

            /// <remarks/>
            public object VOYAGE_NUMBER
            {
                get
                {
                    return this.vOYAGE_NUMBERField;
                }
                set
                {
                    this.vOYAGE_NUMBERField = value;
                }
            }

            /// <remarks/>
            public object FLIGHT_NUMBER
            {
                get
                {
                    return this.fLIGHT_NUMBERField;
                }
                set
                {
                    this.fLIGHT_NUMBERField = value;
                }
            }

            /// <remarks/>
            public object SEAL_NUMBER
            {
                get
                {
                    return this.sEAL_NUMBERField;
                }
                set
                {
                    this.sEAL_NUMBERField = value;
                }
            }

            /// <remarks/>
            public object CONTAINER_NUMBER
            {
                get
                {
                    return this.cONTAINER_NUMBERField;
                }
                set
                {
                    this.cONTAINER_NUMBERField = value;
                }
            }

            /// <remarks/>
            public object TRUCK_SIZE
            {
                get
                {
                    return this.tRUCK_SIZEField;
                }
                set
                {
                    this.tRUCK_SIZEField = value;
                }
            }

            /// <remarks/>
            public object CONTAINER_TYPE
            {
                get
                {
                    return this.cONTAINER_TYPEField;
                }
                set
                {
                    this.cONTAINER_TYPEField = value;
                }
            }

            /// <remarks/>
            public object LOADING_PORT
            {
                get
                {
                    return this.lOADING_PORTField;
                }
                set
                {
                    this.lOADING_PORTField = value;
                }
            }

            /// <remarks/>
            public object INVOICE_NUMBER
            {
                get
                {
                    return this.iNVOICE_NUMBERField;
                }
                set
                {
                    this.iNVOICE_NUMBERField = value;
                }
            }

            /// <remarks/>
            public string AWB_NUMBER
            {
                get
                {
                    return this.aWB_NUMBERField;
                }
                set
                {
                    this.aWB_NUMBERField = value;
                }
            }

            /// <remarks/>
            public object BILL_OF_LADING
            {
                get
                {
                    return this.bILL_OF_LADINGField;
                }
                set
                {
                    this.bILL_OF_LADINGField = value;
                }
            }

            /// <remarks/>
            public object CARRIER_ID
            {
                get
                {
                    return this.cARRIER_IDField;
                }
                set
                {
                    this.cARRIER_IDField = value;
                }
            }

            /// <remarks/>
            public object PRO_NUMBER
            {
                get
                {
                    return this.pRO_NUMBERField;
                }
                set
                {
                    this.pRO_NUMBERField = value;
                }
            }

            /// <remarks/>
            public object RECEIVING_TYPE
            {
                get
                {
                    return this.rECEIVING_TYPEField;
                }
                set
                {
                    this.rECEIVING_TYPEField = value;
                }
            }

            /// <remarks/>
            public object PRIORITY_CODE
            {
                get
                {
                    return this.pRIORITY_CODEField;
                }
                set
                {
                    this.pRIORITY_CODEField = value;
                }
            }

            /// <remarks/>
            public object SPECIAL_INSTRUCTIONS
            {
                get
                {
                    return this.sPECIAL_INSTRUCTIONSField;
                }
                set
                {
                    this.sPECIAL_INSTRUCTIONSField = value;
                }
            }

            /// <remarks/>
            public string ACTION
            {
                get
                {
                    return this.aCTIONField;
                }
                set
                {
                    this.aCTIONField = value;
                }
            }

            /// <remarks/>
            public object LAST_ACTIVITY_DATE
            {
                get
                {
                    return this.lAST_ACTIVITY_DATEField;
                }
                set
                {
                    this.lAST_ACTIVITY_DATEField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("PurchaseOrderDetails")]
            public PurchaseOrdersPurchaseOrderHeaderPurchaseOrderDetails[] PurchaseOrderDetails
            {
                get
                {
                    return this.purchaseOrderDetailsField;
                }
                set
                {
                    this.purchaseOrderDetailsField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class PurchaseOrdersPurchaseOrderHeaderPurchaseOrderDetails
        {

            private string sTATUSField;

            private byte lINE_NUMBERField;

            private byte sHIPMENT_NUMBERField;

            private uint pRODUCTField;

            private object qUALITYField;

            private object lOTField;

            private byte qUANTITY_EXPECTEDField;

            private byte qUANTITY_ALREADY_RECEIVEDField;

            private string uNIT_OF_MEASURE_CODEField;

            private byte cRATING_COSTField;

            private string dELIVER_BYField;

            private byte eXPECTED_DELIVERY_DATEField;

            private byte uNIT_OF_MEASURE_QTYField;

            private byte pRICEField;

            private byte vAS_TYPEField;

            private string dESCRIPTION20Field;

            private string dESCRIPTION50Field;

            private decimal hEIGHTField;

            private decimal wIDTHField;

            private decimal lENGTHField;

            private byte wEIGHTField;

            private string sTYLE_CODEField;

            private byte sIZE_CODEField;

            private string cOLOR_CODEField;

            private object pRODUCT_GROUP_CODEField;

            private string pRODUCT_SUB_GROUP_CODEField;

            private object aCCOUNTING_CODEField;

            private object cOUNTRY_CODEField;

            private object hARMONIZED_TARIFF_CODEField;

            private object sERIAL_TYPE01Field;

            private object sERIAL_TYPE02Field;

            private object sERIAL_TYPE03Field;

            private object sERIAL_TYPE04Field;

            private object sERIAL_TYPE05Field;

            private object sERIAL_TYPE06Field;

            private object sERIAL_TYPE07Field;

            private object sERIAL_TYPE08Field;

            private object sERIAL_TYPE09Field;

            private object sERIAL_TYPE10Field;

            private string lOT_CONTROL_PRODUCTField;

            private ushort nUMBER_OF_DAYS_TO_EXPIRATIONField;

            private string dROP_SHIP_ITEM_FLAGField;

            private string dIVISIONField;

            private string pRODUCT_ABC_CODESField;

            private decimal pRODUCT_VALUEField;

            private string dATE_SEQUENCEField;

            private string rETURNS_ALLOWEDField;

            private string sEASON_CODEField;

            private string rEQUIRES_CUBEField;

            private string hANDLING_CODEField;

            private string fIRST_RECEIVED_DATEField;

            private string hAZMAT_CODEField;

            private string sECURED_LEVELField;

            private string nON_STOCK_ITEMField;

            private string cASE_PICKABLEField;

            private string sPECIAL_HANDLING_CODEField;

            private ushort pALLET_BLOCKField;

            private ushort pALLET_TIERField;

            private byte mAX_NUMBER_PALLET_STACKEDField;

            private string cARTON_PACKEDField;

            private string sLOT_TYPEField;

            private string bAR_CODE_01Field;

            private string bAR_CODE_TYPE_01Field;

            private string bAR_CODE_02Field;

            private string bAR_CODE_TYPE_02Field;

            private string bAR_CODE_03Field;

            private string bAR_CODE_TYPE_03Field;

            private string bAR_CODE_04Field;

            private string bAR_CODE_TYPE_04Field;

            private string bAR_CODE_05Field;

            private string bAR_CODE_TYPE_05Field;

            private string bAR_CODE_06Field;

            private string bAR_CODE_TYPE_06Field;

            private byte bENCHField;

            private string aCTIONField;

            private string lAST_ACTIVITY_DATEField;

            /// <remarks/>
            public string STATUS
            {
                get
                {
                    return this.sTATUSField;
                }
                set
                {
                    this.sTATUSField = value;
                }
            }

            /// <remarks/>
            public byte LINE_NUMBER
            {
                get
                {
                    return this.lINE_NUMBERField;
                }
                set
                {
                    this.lINE_NUMBERField = value;
                }
            }

            /// <remarks/>
            public byte SHIPMENT_NUMBER
            {
                get
                {
                    return this.sHIPMENT_NUMBERField;
                }
                set
                {
                    this.sHIPMENT_NUMBERField = value;
                }
            }

            /// <remarks/>
            public uint PRODUCT
            {
                get
                {
                    return this.pRODUCTField;
                }
                set
                {
                    this.pRODUCTField = value;
                }
            }

            /// <remarks/>
            public object QUALITY
            {
                get
                {
                    return this.qUALITYField;
                }
                set
                {
                    this.qUALITYField = value;
                }
            }

            /// <remarks/>
            public object LOT
            {
                get
                {
                    return this.lOTField;
                }
                set
                {
                    this.lOTField = value;
                }
            }

            /// <remarks/>
            public byte QUANTITY_EXPECTED
            {
                get
                {
                    return this.qUANTITY_EXPECTEDField;
                }
                set
                {
                    this.qUANTITY_EXPECTEDField = value;
                }
            }

            /// <remarks/>
            public byte QUANTITY_ALREADY_RECEIVED
            {
                get
                {
                    return this.qUANTITY_ALREADY_RECEIVEDField;
                }
                set
                {
                    this.qUANTITY_ALREADY_RECEIVEDField = value;
                }
            }

            /// <remarks/>
            public string UNIT_OF_MEASURE_CODE
            {
                get
                {
                    return this.uNIT_OF_MEASURE_CODEField;
                }
                set
                {
                    this.uNIT_OF_MEASURE_CODEField = value;
                }
            }

            /// <remarks/>
            public byte CRATING_COST
            {
                get
                {
                    return this.cRATING_COSTField;
                }
                set
                {
                    this.cRATING_COSTField = value;
                }
            }

            /// <remarks/>
            public string DELIVER_BY
            {
                get
                {
                    return this.dELIVER_BYField;
                }
                set
                {
                    this.dELIVER_BYField = value;
                }
            }

            /// <remarks/>
            public byte EXPECTED_DELIVERY_DATE
            {
                get
                {
                    return this.eXPECTED_DELIVERY_DATEField;
                }
                set
                {
                    this.eXPECTED_DELIVERY_DATEField = value;
                }
            }

            /// <remarks/>
            public byte UNIT_OF_MEASURE_QTY
            {
                get
                {
                    return this.uNIT_OF_MEASURE_QTYField;
                }
                set
                {
                    this.uNIT_OF_MEASURE_QTYField = value;
                }
            }

            /// <remarks/>
            public byte PRICE
            {
                get
                {
                    return this.pRICEField;
                }
                set
                {
                    this.pRICEField = value;
                }
            }

            /// <remarks/>
            public byte VAS_TYPE
            {
                get
                {
                    return this.vAS_TYPEField;
                }
                set
                {
                    this.vAS_TYPEField = value;
                }
            }

            /// <remarks/>
            public string DESCRIPTION20
            {
                get
                {
                    return this.dESCRIPTION20Field;
                }
                set
                {
                    this.dESCRIPTION20Field = value;
                }
            }

            /// <remarks/>
            public string DESCRIPTION50
            {
                get
                {
                    return this.dESCRIPTION50Field;
                }
                set
                {
                    this.dESCRIPTION50Field = value;
                }
            }

            /// <remarks/>
            public decimal HEIGHT
            {
                get
                {
                    return this.hEIGHTField;
                }
                set
                {
                    this.hEIGHTField = value;
                }
            }

            /// <remarks/>
            public decimal WIDTH
            {
                get
                {
                    return this.wIDTHField;
                }
                set
                {
                    this.wIDTHField = value;
                }
            }

            /// <remarks/>
            public decimal LENGTH
            {
                get
                {
                    return this.lENGTHField;
                }
                set
                {
                    this.lENGTHField = value;
                }
            }

            /// <remarks/>
            public byte WEIGHT
            {
                get
                {
                    return this.wEIGHTField;
                }
                set
                {
                    this.wEIGHTField = value;
                }
            }

            /// <remarks/>
            public string STYLE_CODE
            {
                get
                {
                    return this.sTYLE_CODEField;
                }
                set
                {
                    this.sTYLE_CODEField = value;
                }
            }

            /// <remarks/>
            public byte SIZE_CODE
            {
                get
                {
                    return this.sIZE_CODEField;
                }
                set
                {
                    this.sIZE_CODEField = value;
                }
            }

            /// <remarks/>
            public string COLOR_CODE
            {
                get
                {
                    return this.cOLOR_CODEField;
                }
                set
                {
                    this.cOLOR_CODEField = value;
                }
            }

            /// <remarks/>
            public object PRODUCT_GROUP_CODE
            {
                get
                {
                    return this.pRODUCT_GROUP_CODEField;
                }
                set
                {
                    this.pRODUCT_GROUP_CODEField = value;
                }
            }

            /// <remarks/>
            public string PRODUCT_SUB_GROUP_CODE
            {
                get
                {
                    return this.pRODUCT_SUB_GROUP_CODEField;
                }
                set
                {
                    this.pRODUCT_SUB_GROUP_CODEField = value;
                }
            }

            /// <remarks/>
            public object ACCOUNTING_CODE
            {
                get
                {
                    return this.aCCOUNTING_CODEField;
                }
                set
                {
                    this.aCCOUNTING_CODEField = value;
                }
            }

            /// <remarks/>
            public object COUNTRY_CODE
            {
                get
                {
                    return this.cOUNTRY_CODEField;
                }
                set
                {
                    this.cOUNTRY_CODEField = value;
                }
            }

            /// <remarks/>
            public object HARMONIZED_TARIFF_CODE
            {
                get
                {
                    return this.hARMONIZED_TARIFF_CODEField;
                }
                set
                {
                    this.hARMONIZED_TARIFF_CODEField = value;
                }
            }

            /// <remarks/>
            public object SERIAL_TYPE01
            {
                get
                {
                    return this.sERIAL_TYPE01Field;
                }
                set
                {
                    this.sERIAL_TYPE01Field = value;
                }
            }

            /// <remarks/>
            public object SERIAL_TYPE02
            {
                get
                {
                    return this.sERIAL_TYPE02Field;
                }
                set
                {
                    this.sERIAL_TYPE02Field = value;
                }
            }

            /// <remarks/>
            public object SERIAL_TYPE03
            {
                get
                {
                    return this.sERIAL_TYPE03Field;
                }
                set
                {
                    this.sERIAL_TYPE03Field = value;
                }
            }

            /// <remarks/>
            public object SERIAL_TYPE04
            {
                get
                {
                    return this.sERIAL_TYPE04Field;
                }
                set
                {
                    this.sERIAL_TYPE04Field = value;
                }
            }

            /// <remarks/>
            public object SERIAL_TYPE05
            {
                get
                {
                    return this.sERIAL_TYPE05Field;
                }
                set
                {
                    this.sERIAL_TYPE05Field = value;
                }
            }

            /// <remarks/>
            public object SERIAL_TYPE06
            {
                get
                {
                    return this.sERIAL_TYPE06Field;
                }
                set
                {
                    this.sERIAL_TYPE06Field = value;
                }
            }

            /// <remarks/>
            public object SERIAL_TYPE07
            {
                get
                {
                    return this.sERIAL_TYPE07Field;
                }
                set
                {
                    this.sERIAL_TYPE07Field = value;
                }
            }

            /// <remarks/>
            public object SERIAL_TYPE08
            {
                get
                {
                    return this.sERIAL_TYPE08Field;
                }
                set
                {
                    this.sERIAL_TYPE08Field = value;
                }
            }

            /// <remarks/>
            public object SERIAL_TYPE09
            {
                get
                {
                    return this.sERIAL_TYPE09Field;
                }
                set
                {
                    this.sERIAL_TYPE09Field = value;
                }
            }

            /// <remarks/>
            public object SERIAL_TYPE10
            {
                get
                {
                    return this.sERIAL_TYPE10Field;
                }
                set
                {
                    this.sERIAL_TYPE10Field = value;
                }
            }

            /// <remarks/>
            public string LOT_CONTROL_PRODUCT
            {
                get
                {
                    return this.lOT_CONTROL_PRODUCTField;
                }
                set
                {
                    this.lOT_CONTROL_PRODUCTField = value;
                }
            }

            /// <remarks/>
            public ushort NUMBER_OF_DAYS_TO_EXPIRATION
            {
                get
                {
                    return this.nUMBER_OF_DAYS_TO_EXPIRATIONField;
                }
                set
                {
                    this.nUMBER_OF_DAYS_TO_EXPIRATIONField = value;
                }
            }

            /// <remarks/>
            public string DROP_SHIP_ITEM_FLAG
            {
                get
                {
                    return this.dROP_SHIP_ITEM_FLAGField;
                }
                set
                {
                    this.dROP_SHIP_ITEM_FLAGField = value;
                }
            }

            /// <remarks/>
            public string DIVISION
            {
                get
                {
                    return this.dIVISIONField;
                }
                set
                {
                    this.dIVISIONField = value;
                }
            }

            /// <remarks/>
            public string PRODUCT_ABC_CODES
            {
                get
                {
                    return this.pRODUCT_ABC_CODESField;
                }
                set
                {
                    this.pRODUCT_ABC_CODESField = value;
                }
            }

            /// <remarks/>
            public decimal PRODUCT_VALUE
            {
                get
                {
                    return this.pRODUCT_VALUEField;
                }
                set
                {
                    this.pRODUCT_VALUEField = value;
                }
            }

            /// <remarks/>
            public string DATE_SEQUENCE
            {
                get
                {
                    return this.dATE_SEQUENCEField;
                }
                set
                {
                    this.dATE_SEQUENCEField = value;
                }
            }

            /// <remarks/>
            public string RETURNS_ALLOWED
            {
                get
                {
                    return this.rETURNS_ALLOWEDField;
                }
                set
                {
                    this.rETURNS_ALLOWEDField = value;
                }
            }

            /// <remarks/>
            public string SEASON_CODE
            {
                get
                {
                    return this.sEASON_CODEField;
                }
                set
                {
                    this.sEASON_CODEField = value;
                }
            }

            /// <remarks/>
            public string REQUIRES_CUBE
            {
                get
                {
                    return this.rEQUIRES_CUBEField;
                }
                set
                {
                    this.rEQUIRES_CUBEField = value;
                }
            }

            /// <remarks/>
            public string HANDLING_CODE
            {
                get
                {
                    return this.hANDLING_CODEField;
                }
                set
                {
                    this.hANDLING_CODEField = value;
                }
            }

            /// <remarks/>
            public string FIRST_RECEIVED_DATE
            {
                get
                {
                    return this.fIRST_RECEIVED_DATEField;
                }
                set
                {
                    this.fIRST_RECEIVED_DATEField = value;
                }
            }

            /// <remarks/>
            public string HAZMAT_CODE
            {
                get
                {
                    return this.hAZMAT_CODEField;
                }
                set
                {
                    this.hAZMAT_CODEField = value;
                }
            }

            /// <remarks/>
            public string SECURED_LEVEL
            {
                get
                {
                    return this.sECURED_LEVELField;
                }
                set
                {
                    this.sECURED_LEVELField = value;
                }
            }

            /// <remarks/>
            public string NON_STOCK_ITEM
            {
                get
                {
                    return this.nON_STOCK_ITEMField;
                }
                set
                {
                    this.nON_STOCK_ITEMField = value;
                }
            }

            /// <remarks/>
            public string CASE_PICKABLE
            {
                get
                {
                    return this.cASE_PICKABLEField;
                }
                set
                {
                    this.cASE_PICKABLEField = value;
                }
            }

            /// <remarks/>
            public string SPECIAL_HANDLING_CODE
            {
                get
                {
                    return this.sPECIAL_HANDLING_CODEField;
                }
                set
                {
                    this.sPECIAL_HANDLING_CODEField = value;
                }
            }

            /// <remarks/>
            public ushort PALLET_BLOCK
            {
                get
                {
                    return this.pALLET_BLOCKField;
                }
                set
                {
                    this.pALLET_BLOCKField = value;
                }
            }

            /// <remarks/>
            public ushort PALLET_TIER
            {
                get
                {
                    return this.pALLET_TIERField;
                }
                set
                {
                    this.pALLET_TIERField = value;
                }
            }

            /// <remarks/>
            public byte MAX_NUMBER_PALLET_STACKED
            {
                get
                {
                    return this.mAX_NUMBER_PALLET_STACKEDField;
                }
                set
                {
                    this.mAX_NUMBER_PALLET_STACKEDField = value;
                }
            }

            /// <remarks/>
            public string CARTON_PACKED
            {
                get
                {
                    return this.cARTON_PACKEDField;
                }
                set
                {
                    this.cARTON_PACKEDField = value;
                }
            }

            /// <remarks/>
            public string SLOT_TYPE
            {
                get
                {
                    return this.sLOT_TYPEField;
                }
                set
                {
                    this.sLOT_TYPEField = value;
                }
            }

            /// <remarks/>
            public string BAR_CODE_01
            {
                get
                {
                    return this.bAR_CODE_01Field;
                }
                set
                {
                    this.bAR_CODE_01Field = value;
                }
            }

            /// <remarks/>
            public string BAR_CODE_TYPE_01
            {
                get
                {
                    return this.bAR_CODE_TYPE_01Field;
                }
                set
                {
                    this.bAR_CODE_TYPE_01Field = value;
                }
            }

            /// <remarks/>
            public string BAR_CODE_02
            {
                get
                {
                    return this.bAR_CODE_02Field;
                }
                set
                {
                    this.bAR_CODE_02Field = value;
                }
            }

            /// <remarks/>
            public string BAR_CODE_TYPE_02
            {
                get
                {
                    return this.bAR_CODE_TYPE_02Field;
                }
                set
                {
                    this.bAR_CODE_TYPE_02Field = value;
                }
            }

            /// <remarks/>
            public string BAR_CODE_03
            {
                get
                {
                    return this.bAR_CODE_03Field;
                }
                set
                {
                    this.bAR_CODE_03Field = value;
                }
            }

            /// <remarks/>
            public string BAR_CODE_TYPE_03
            {
                get
                {
                    return this.bAR_CODE_TYPE_03Field;
                }
                set
                {
                    this.bAR_CODE_TYPE_03Field = value;
                }
            }

            /// <remarks/>
            public string BAR_CODE_04
            {
                get
                {
                    return this.bAR_CODE_04Field;
                }
                set
                {
                    this.bAR_CODE_04Field = value;
                }
            }

            /// <remarks/>
            public string BAR_CODE_TYPE_04
            {
                get
                {
                    return this.bAR_CODE_TYPE_04Field;
                }
                set
                {
                    this.bAR_CODE_TYPE_04Field = value;
                }
            }

            /// <remarks/>
            public string BAR_CODE_05
            {
                get
                {
                    return this.bAR_CODE_05Field;
                }
                set
                {
                    this.bAR_CODE_05Field = value;
                }
            }

            /// <remarks/>
            public string BAR_CODE_TYPE_05
            {
                get
                {
                    return this.bAR_CODE_TYPE_05Field;
                }
                set
                {
                    this.bAR_CODE_TYPE_05Field = value;
                }
            }

            /// <remarks/>
            public string BAR_CODE_06
            {
                get
                {
                    return this.bAR_CODE_06Field;
                }
                set
                {
                    this.bAR_CODE_06Field = value;
                }
            }

            /// <remarks/>
            public string BAR_CODE_TYPE_06
            {
                get
                {
                    return this.bAR_CODE_TYPE_06Field;
                }
                set
                {
                    this.bAR_CODE_TYPE_06Field = value;
                }
            }

            /// <remarks/>
            public byte BENCH
            {
                get
                {
                    return this.bENCHField;
                }
                set
                {
                    this.bENCHField = value;
                }
            }

            /// <remarks/>
            public string ACTION
            {
                get
                {
                    return this.aCTIONField;
                }
                set
                {
                    this.aCTIONField = value;
                }
            }

            /// <remarks/>
            public string LAST_ACTIVITY_DATE
            {
                get
                {
                    return this.lAST_ACTIVITY_DATEField;
                }
                set
                {
                    this.lAST_ACTIVITY_DATEField = value;
                }
            }
        }



    }
}
