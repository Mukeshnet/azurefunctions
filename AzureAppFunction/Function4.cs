using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;

namespace AzureAppFunction
{
    public static class Function4
    {
        [FunctionName("Function4")]
        public static async Task<HttpResponseMessage> Run([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            DataSet dataset = new DataSet();
            string OutputType = req.Headers.GetValues("type").FirstOrDefault();
            string purchaseorederResult;
            string connectionString = "Data Source= 54.85.106.196; Initial Catalog= Lj_link; User ID=larson; pwd=larson; Integrated Security=False";
            using (SqlConnection connection =
                new SqlConnection(connectionString))
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = new SqlCommand(
                    "Select top 10 * from PurchaseOrderHeader", connection);
                adapter.Fill(dataset);
                if (OutputType?.ToLower() == "xml")
                {
                    purchaseorederResult = dataset.GetXml().Replace("\r\n", "");
                    return req.CreateResponse(HttpStatusCode.OK, purchaseorederResult);
                }
                else
                {
                    purchaseorederResult = JsonConvert.SerializeObject(dataset.Tables[0]);
                    return req.CreateResponse(HttpStatusCode.OK, JsonConvert.DeserializeObject<object>(purchaseorederResult));
                }
            }


        //    string constr = "Data Source= 54.85.106.196; Initial Catalog= Lj_link; User ID=larson; pwd=larson; Integrated Security=False";
        //    using (SqlConnection con = new SqlConnection(constr))
        //    {
        //        using (SqlCommand cmd = new SqlCommand("Select top 10 * from PurchaseOrderHeader"))
        //        {
        //            using (SqlDataAdapter sda = new SqlDataAdapter())
        //            {
        //                cmd.Connection = con;
        //                sda.SelectCommand = cmd;
        //                using (DataTable dt = new DataTable())
        //                {
        //                    sda.Fill(dt);

        //                    //Build the CSV file data as a Comma separated string.
        //                    string csv = string.Empty;

        //                    foreach (DataColumn column in dt.Columns)
        //                    {
        //                        //Add the Header row for CSV file.
        //                        csv += column.ColumnName + ',';
        //                    }

        //                    //Add new line.
        //                    csv += "\r\n";

        //                    foreach (DataRow row in dt.Rows)
        //                    {
        //                        foreach (DataColumn column in dt.Columns)
        //                        {
        //                            //Add the Data rows.
        //                            csv += row[column.ColumnName].ToString().Replace(",", ";") + ',';
        //                        }

        //                        //Add new line.
        //                        csv += "\r\n";
        //                    }

        //                    //Download the CSV file.
        //                    Response.Clear();
        //                    Response.Buffer = true;
        //                    Response.AddHeader("content-disposition", "attachment;filename=SqlExport.csv");
        //                    Response.Charset = "";
        //                    Response.ContentType = "application/text";
        //                    Response.Output.Write(csv);
        //                    Response.Flush();
        //                    Response.End();
        //                }
        //            }
        //        }
        //    }

        }
    }
}
