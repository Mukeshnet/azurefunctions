using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using System.Data.SqlClient;
using System.Data;

namespace AzureAppFunction
{
    public static class Function2
    {
        [FunctionName("Function2")]
        public static async Task<HttpResponseMessage> Run([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            DataSet dataset = new DataSet();
            using (SqlConnection connection =
                new SqlConnection("Data Source= 54.85.106.196; Initial Catalog= ljwebsite; User ID=larson; pwd=larson; Integrated Security=False"))
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = new SqlCommand(
                    "SELECT TOP 500 * FROM ADDRESS", connection);
                adapter.Fill(dataset);
                return req.CreateResponse(HttpStatusCode.OK, dataset);

            }
        }
    }
}
